<?php

/**
 * @file
 * Acquia SDK module drush commands.
 */

/**
 * Implements hook_sql_sync_sanitize().
 */
function acquiasdk_drush_sql_sync_sanitize($source) {
  drush_sql_register_post_sync_op('acquiasdk',
    dt('Remove Acquia SDK configuration.'),
    "DELETE FROM variable WHERE name IN ('acquiasdk_cloudapi_password', 'acquiasdk_network_key');"
  );
}
